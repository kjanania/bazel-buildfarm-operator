package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// BazelBuildServerSpec defines the desired state of BazelBuildServer
type BazelBuildServerSpec struct {
	// INSERT ADDITIONAL SPEC FIELDS - desired state of cluster
	// Important: Run "operator-sdk generate k8s" to regenerate code after modifying this file
	// Add custom validation using kubebuilder tags: https://book-v1.book.kubebuilder.io/beyond_basics/generating_crd.html
	BuildfarmServerDeploymentSpec *BuildfarmServerDeploymentSpec `json:"bazelServerSpec,omitempty"`
	BuildfarmWorkerDeploymentSpec *BuildfarmWorkerDeploymentSpec `json:"bazelWorkerSpec,omitempty"`
}

// BuildfarmServerDeploymentSpec comment
type BuildfarmServerDeploymentSpec struct {
	BazelBuildfarmContainerSpec *BazelBuildfarmContainer `json:"bazel,omitempty"`
	RedisSpec                   *RedisContainer          `json:"redis,omitempty"`
}

// BuildfarmWorkerDeploymentSpec comment
type BuildfarmWorkerDeploymentSpec struct {
	Replicas int32 `json:"replicas,omitempty"`
}

// BazelBuildfarmContainer allows to override some of the container config for the Bazel Buildfarm container
type BazelBuildfarmContainer struct {
	BazelBuildfarmImage *string `json:"image,omitempty"`
	BazelBuildfarmPort  int32   `json:"port,omitempty"`
}

// RedisContainer allows to override the container image for the Redis container
type RedisContainer struct {
	RedisImage *string `json:"image,omitempty"`
}

// BazelBuildServerStatus defines the observed state of BazelBuildServer
type BazelBuildServerStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "operator-sdk generate k8s" to regenerate code after modifying this file
	// Add custom validation using kubebuilder tags: https://book-v1.book.kubebuilder.io/beyond_basics/generating_crd.html
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// BazelBuildServer is the Schema for the bazelbuildservers API
// +kubebuilder:subresource:status
// +kubebuilder:resource:path=bazelbuildservers,scope=Namespaced
type BazelBuildServer struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   BazelBuildServerSpec   `json:"spec,omitempty"`
	Status BazelBuildServerStatus `json:"status,omitempty"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// BazelBuildServerList contains a list of BazelBuildServer
type BazelBuildServerList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []BazelBuildServer `json:"items"`
}

func init() {
	SchemeBuilder.Register(&BazelBuildServer{}, &BazelBuildServerList{})
}

// SetDefaults comment
func (bazelBuildServer *BazelBuildServer) SetDefaults() bool {
	defaultBazelImage := "80dw/buildfarm-server:latest"
	defaultRedisImage := "redis:5.0.9"
	defaultBazelPort := int32(8080)
	defaultBuildfarmContainerSpec := BazelBuildfarmContainer{
		BazelBuildfarmImage: &defaultBazelImage,
		BazelBuildfarmPort:  defaultBazelPort,
	}
	defaultRedisSpec := RedisContainer{
		RedisImage: &defaultRedisImage,
	}
	defaultWorkerSpec := BuildfarmWorkerDeploymentSpec{
		Replicas: 1,
	}

	changed := false
	if bazelBuildServer.Spec.BuildfarmServerDeploymentSpec != nil {
		if bazelBuildServer.Spec.BuildfarmServerDeploymentSpec.BazelBuildfarmContainerSpec != nil {
			if bazelBuildServer.Spec.BuildfarmServerDeploymentSpec.BazelBuildfarmContainerSpec.BazelBuildfarmImage == nil {
				bazelBuildServer.Spec.BuildfarmServerDeploymentSpec.BazelBuildfarmContainerSpec.BazelBuildfarmImage =
					&defaultBazelImage
				changed = true
			}
			if bazelBuildServer.Spec.BuildfarmServerDeploymentSpec.BazelBuildfarmContainerSpec.BazelBuildfarmPort == 0 {
				bazelBuildServer.Spec.BuildfarmServerDeploymentSpec.BazelBuildfarmContainerSpec.BazelBuildfarmPort = defaultBazelPort
				changed = true
			}
		} else {
			bazelBuildServer.Spec.BuildfarmServerDeploymentSpec.BazelBuildfarmContainerSpec = &defaultBuildfarmContainerSpec
			changed = true
		}

		if bazelBuildServer.Spec.BuildfarmServerDeploymentSpec.RedisSpec != nil {
			if bazelBuildServer.Spec.BuildfarmServerDeploymentSpec.RedisSpec.RedisImage == nil {
				bazelBuildServer.Spec.BuildfarmServerDeploymentSpec.RedisSpec.RedisImage = &defaultRedisImage
				changed = true
			}
		} else {
			bazelBuildServer.Spec.BuildfarmServerDeploymentSpec.RedisSpec = &defaultRedisSpec
			changed = true
		}
	} else {
		bazelBuildServer.Spec.BuildfarmServerDeploymentSpec = &BuildfarmServerDeploymentSpec{
			BazelBuildfarmContainerSpec: &defaultBuildfarmContainerSpec,
			RedisSpec:                   &defaultRedisSpec,
		}
		changed = true
	}

	if bazelBuildServer.Spec.BuildfarmWorkerDeploymentSpec == nil {
		bazelBuildServer.Spec.BuildfarmWorkerDeploymentSpec = &defaultWorkerSpec
		changed = true
	}

	return changed
}
