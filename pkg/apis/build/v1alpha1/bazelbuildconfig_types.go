package v1alpha1

import (
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// BazelBuildConfigSpec defines the desired state of BazelBuildConfig
type BazelBuildConfigSpec struct {
	// INSERT ADDITIONAL SPEC FIELDS - desired state of cluster
	// Important: Run "operator-sdk generate k8s" to regenerate code after modifying this file
	// Add custom validation using kubebuilder tags: https://book-v1.book.kubebuilder.io/beyond_basics/generating_crd.html
	// Source string `json:"source"`
	// //+optional
	// Revision string        `json:"revision,omitempty"`
	// Strategy BuildStrategy `json:"strategy"`

	// Commented for now
	//triggers determine how new Builds can be launched from a BuildConfig. If
	//no triggers are defined, a new build can only occur as a result of an
	//explicit client build creation.
	// Triggers []BuildTriggerPolicy `json:"triggers" protobuf:"bytes,1,rep,name=triggers"`

	// Commented for now
	// RunPolicy describes how the new build created from this build
	// configuration will be scheduled for execution.
	// This is optional, if not specified we default to "Serial".
	// RunPolicy BuildRunPolicy `json:"runPolicy,omitempty" protobuf:"bytes,2,opt,name=runPolicy,casttype=BuildRunPolicy"`

	// CommonSpec is the desired build specification
	CommonSpec `json:",inline" protobuf:"bytes,3,opt,name=commonSpec"`

	// successfulBuildsHistoryLimit is the number of old successful builds to retain.
	// If not specified, all successful builds are retained.
	SuccessfulBuildsHistoryLimit *int32 `json:"successfulBuildsHistoryLimit,omitempty" protobuf:"varint,4,opt,name=successfulBuildsHistoryLimit"`

	// failedBuildsHistoryLimit is the number of old failed builds to retain.
	// If not specified, all failed builds are retained.
	FailedBuildsHistoryLimit *int32 `json:"failedBuildsHistoryLimit,omitempty" protobuf:"varint,5,opt,name=failedBuildsHistoryLimit"`
}

// CommonSpec encapsulates all the inputs necessary to represent a build.
type CommonSpec struct {
	// serviceAccount is the name of the ServiceAccount to use to run the pod
	// created by this build.
	// The pod will be allowed to use secrets referenced by the ServiceAccount
	ServiceAccount string `json:"serviceAccount,omitempty" protobuf:"bytes,1,opt,name=serviceAccount"`

	// source describes the SCM in use.
	Source BuildSource `json:"source,omitempty" protobuf:"bytes,2,opt,name=source"`

	// revision is the information from the source for a specific repo snapshot.
	// This is optional.
	Revision *SourceRevision `json:"revision,omitempty" protobuf:"bytes,3,opt,name=revision"`

	// strategy defines how to perform a build.
	Strategy BuildStrategy `json:"strategy" protobuf:"bytes,4,opt,name=strategy"`

	// output describes the Docker image the Strategy should produce.
	Output BuildOutput `json:"output,omitempty" protobuf:"bytes,5,opt,name=output"`

	// resources computes resource requirements to execute the build.
	Resources v1.ResourceRequirements `json:"resources,omitempty" protobuf:"bytes,6,opt,name=resources"`

	// Commented for now
	// postCommit is a build hook executed after the build output image is
	// committed, before it is pushed to a registry.
	// PostCommit BuildPostCommitSpec `json:"postCommit,omitempty" protobuf:"bytes,7,opt,name=postCommit"`

	// completionDeadlineSeconds is an optional duration in seconds, counted from
	// the time when a build pod gets scheduled in the system, that the build may
	// be active on a node before the system actively tries to terminate the
	// build; value must be positive integer
	CompletionDeadlineSeconds *int64 `json:"completionDeadlineSeconds,omitempty" protobuf:"varint,8,opt,name=completionDeadlineSeconds"`

	// Commented for now
	// nodeSelector is a selector which must be true for the build pod to fit on a node
	// If nil, it can be overridden by default build nodeselector values for the cluster.
	// If set to an empty map or a map with any values, default build nodeselector values
	// are ignored.
	// NodeSelector OptionalNodeSelector `json:"nodeSelector" protobuf:"bytes,9,name=nodeSelector"`
}

// BuildSource is the SCM used for the build.
// Limiting it to Source builds for now
type BuildSource struct {
	// type of build input to accept
	// +kubebuilder:validation:Enum=Git
	Type string `json:"type"`

	// binary builds accept a binary as their input. The binary is generally assumed to be a tar,
	// gzipped tar, or zip file depending on the strategy. For Docker builds, this is the build
	// context and an optional Dockerfile may be specified to override any Dockerfile in the
	// build context. For Source builds, this is assumed to be an archive as described above. For
	// Source and Docker builds, if binary.asFile is set the build will receive a directory with
	// a single file. contextDir may be used when an archive is provided. Custom builds will
	// receive this binary as input on STDIN.
	// Binary *BinaryBuildSource `json:"binary,omitempty" protobuf:"bytes,2,opt,name=binary"`

	// dockerfile is the raw contents of a Dockerfile which should be built. When this option is
	// specified, the FROM may be modified based on your strategy base image and additional ENV
	// stanzas from your strategy environment will be added after the FROM, but before the rest
	// of your Dockerfile stanzas. The Dockerfile source type may be used with other options like
	// git - in those cases the Git repo will have any innate Dockerfile replaced in the context
	// dir.
	// Dockerfile *string `json:"dockerfile,omitempty" protobuf:"bytes,3,opt,name=dockerfile"`

	// git contains optional information about git build source
	Git *GitBuildSource `json:"git,omitempty" protobuf:"bytes,4,opt,name=git"`

	// images describes a set of images to be used to provide source for the build
	// Images []ImageSource `json:"images,omitempty" protobuf:"bytes,5,rep,name=images"`

	// contextDir specifies the sub-directory where the source code for the application exists.
	// This allows to have buildable sources in directory other than root of
	// repository.
	ContextDir string `json:"contextDir,omitempty" protobuf:"bytes,6,opt,name=contextDir"`

	// sourceSecret is the name of a Secret that would be used for setting
	// up the authentication for cloning private repository.
	// The secret contains valid credentials for remote repository, where the
	// data's key represent the authentication method to be used and value is
	// the base64 encoded credentials. Supported auth methods are: ssh-privatekey.
	SourceSecret *v1.LocalObjectReference `json:"sourceSecret,omitempty" protobuf:"bytes,7,opt,name=sourceSecret"`

	// secrets represents a list of secrets and their destinations that will
	// be used only for the build.
	Secrets []SecretBuildSource `json:"secrets,omitempty" protobuf:"bytes,8,rep,name=secrets"`
}

// GitBuildSource defines the parameters of a Git SCM
type GitBuildSource struct {
	// uri points to the source that will be built. The structure of the source
	// will depend on the type of build to run
	URI string `json:"uri" protobuf:"bytes,1,opt,name=uri"`

	// ref is the branch/tag/ref to build.
	Ref string `json:"ref,omitempty" protobuf:"bytes,2,opt,name=ref"`

	// proxyConfig defines the proxies to use for the git clone operation
	ProxyConfig `json:",inline" protobuf:"bytes,3,opt,name=proxyConfig"`
}

// SecretBuildSource describes a secret and its destination directory that will be
// used only at the build time. The content of the secret referenced here will
// be copied into the destination directory instead of mounting.
type SecretBuildSource struct {
	// secret is a reference to an existing secret that you want to use in your
	// build.
	Secret v1.LocalObjectReference `json:"secret" protobuf:"bytes,1,opt,name=secret"`

	// destinationDir is the directory where the files from the secret should be
	// available for the build time.
	// For the Source build strategy, these will be injected into a container
	// where the assemble script runs. Later, when the script finishes, all files
	// injected will be truncated to zero length.
	// For the Docker build strategy, these will be copied into the build
	// directory, where the Dockerfile is located, so users can ADD or COPY them
	// during docker build.
	DestinationDir string `json:"destinationDir,omitempty" protobuf:"bytes,2,opt,name=destinationDir"`
}

// ProxyConfig defines what proxies to use for an operation
type ProxyConfig struct {
	// httpProxy is a proxy used to reach the git repository over http
	HTTPProxy *string `json:"httpProxy,omitempty" protobuf:"bytes,3,opt,name=httpProxy"`

	// httpsProxy is a proxy used to reach the git repository over https
	HTTPSProxy *string `json:"httpsProxy,omitempty" protobuf:"bytes,4,opt,name=httpsProxy"`

	// noProxy is the list of domains for which the proxy should not be used
	NoProxy *string `json:"noProxy,omitempty" protobuf:"bytes,5,opt,name=noProxy"`
}

// SourceRevision is the revision or commit information from the source for the build
type SourceRevision struct {
	// type of build source input to accept
	// Only accept Source for now
	// +kubebuilder:validation:Enum=Source
	Type string `json:"type"`

	// Git contains information about git-based build source
	Git *GitSourceRevision `json:"git,omitempty" protobuf:"bytes,2,opt,name=git"`
}

// GitSourceRevision is the commit information from a git source for a build
type GitSourceRevision struct {
	// commit is the commit hash identifying a specific commit
	Commit string `json:"commit,omitempty" protobuf:"bytes,1,opt,name=commit"`

	// author is the author of a specific commit
	Author SourceControlUser `json:"author,omitempty" protobuf:"bytes,2,opt,name=author"`

	// committer is the committer of a specific commit
	Committer SourceControlUser `json:"committer,omitempty" protobuf:"bytes,3,opt,name=committer"`

	// message is the description of a specific commit
	Message string `json:"message,omitempty" protobuf:"bytes,4,opt,name=message"`
}

// SourceControlUser defines the identity of a user of source control
type SourceControlUser struct {
	// name of the source control user
	Name string `json:"name,omitempty" protobuf:"bytes,1,opt,name=name"`

	// email of the source control user
	Email string `json:"email,omitempty" protobuf:"bytes,2,opt,name=email"`
}

// BuildOutput is input to a build strategy and describes the Docker image that the strategy
// should produce.
type BuildOutput struct {
	// to defines an optional location to push the output of this build to.
	// Kind must be one of 'ImageStreamTag' or 'DockerImage'.
	// This value will be used to look up a Docker image repository to push to.
	// In the case of an ImageStreamTag, the ImageStreamTag will be looked for in the namespace of
	// the build unless Namespace is specified.
	To *v1.ObjectReference `json:"to,omitempty" protobuf:"bytes,1,opt,name=to"`

	// PushSecret is the name of a Secret that would be used for setting
	// up the authentication for executing the Docker push to authentication
	// enabled Docker Registry (or Docker Hub).
	PushSecret *v1.LocalObjectReference `json:"pushSecret,omitempty" protobuf:"bytes,2,opt,name=pushSecret"`

	// imageLabels define a list of labels that are applied to the resulting image. If there
	// are multiple labels with the same name then the last one in the list is used.
	ImageLabels []ImageLabel `json:"imageLabels,omitempty" protobuf:"bytes,3,rep,name=imageLabels"`
}

// ImageLabel represents a label applied to the resulting image.
type ImageLabel struct {
	// name defines the name of the label. It must have non-zero length.
	Name string `json:"name" protobuf:"bytes,1,opt,name=name"`

	// value defines the literal value of the label.
	Value string `json:"value,omitempty" protobuf:"bytes,2,opt,name=value"`
}

// BuildStrategy contains the details of how to perform a build.
// Only accept SourceStrategy for now
type BuildStrategy struct {
	// type of build strategy input to accept
	// Only accept Source for now
	// +kubebuilder:validation:Enum=Source
	Type string `json:"type"`

	// dockerStrategy holds the parameters to the Docker build strategy.
	// DockerStrategy *DockerBuildStrategy `json:"dockerStrategy,omitempty" protobuf:"bytes,2,opt,name=dockerStrategy"`

	// sourceStrategy holds the parameters to the Source build strategy.
	SourceStrategy *SourceBuildStrategy `json:"sourceStrategy,omitempty" protobuf:"bytes,3,opt,name=sourceStrategy"`

	// customStrategy holds the parameters to the Custom build strategy
	// CustomStrategy *CustomBuildStrategy `json:"customStrategy,omitempty" protobuf:"bytes,4,opt,name=customStrategy"`

	// JenkinsPipelineStrategy holds the parameters to the Jenkins Pipeline build strategy.
	// JenkinsPipelineStrategy *JenkinsPipelineBuildStrategy `json:"jenkinsPipelineStrategy,omitempty" protobuf:"bytes,5,opt,name=jenkinsPipelineStrategy"`
}

// SourceBuildStrategy defines input parameters specific to an Source build.
type SourceBuildStrategy struct {
	// from is reference to an DockerImage, ImageStreamTag, or ImageStreamImage from which
	// the docker image should be pulled
	From v1.ObjectReference `json:"from" protobuf:"bytes,1,opt,name=from"`

	// pullSecret is the name of a Secret that would be used for setting up
	// the authentication for pulling the Docker images from the private Docker
	// registries
	PullSecret *v1.LocalObjectReference `json:"pullSecret,omitempty" protobuf:"bytes,2,opt,name=pullSecret"`

	// env contains additional environment variables you want to pass into a builder container.
	Env []v1.EnvVar `json:"env,omitempty" protobuf:"bytes,3,rep,name=env"`

	// scripts is the location of Source scripts
	Scripts string `json:"scripts,omitempty" protobuf:"bytes,4,opt,name=scripts"`

	// incremental flag forces the Source build to do incremental builds if true.
	Incremental *bool `json:"incremental,omitempty" protobuf:"varint,5,opt,name=incremental"`

	// forcePull describes if the builder should pull the images from registry prior to building.
	ForcePull bool `json:"forcePull,omitempty" protobuf:"varint,6,opt,name=forcePull"`

	// deprecated json field, do not reuse: runtimeImage
	// +k8s:protobuf-deprecated=runtimeImage,7

	// deprecated json field, do not reuse: runtimeArtifacts
	// +k8s:protobuf-deprecated=runtimeArtifacts,8

}

// BazelBuildConfigStatus defines the observed state of BazelBuildConfig
type BazelBuildConfigStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "operator-sdk generate k8s" to regenerate code after modifying this file
	// Add custom validation using kubebuilder tags: https://book-v1.book.kubebuilder.io/beyond_basics/generating_crd.html
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// BazelBuildConfig is the Schema for the bazelbuildconfigs API
// +kubebuilder:subresource:status
// +kubebuilder:resource:path=bazelbuildconfigs,scope=Namespaced
type BazelBuildConfig struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   BazelBuildConfigSpec   `json:"spec,omitempty"`
	Status BazelBuildConfigStatus `json:"status,omitempty"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// BazelBuildConfigList contains a list of BazelBuildConfig
type BazelBuildConfigList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []BazelBuildConfig `json:"items"`
}

func init() {
	SchemeBuilder.Register(&BazelBuildConfig{}, &BazelBuildConfigList{})
}
