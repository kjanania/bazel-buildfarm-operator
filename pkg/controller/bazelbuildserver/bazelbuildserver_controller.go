package bazelbuildserver

import (
	"context"
	"fmt"
	"strconv"

	buildv1alpha1 "gitlab.com/kjanania/bazel-buildfarm-operator/pkg/apis/build/v1alpha1"
	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/apimachinery/pkg/util/intstr"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

var log = logf.Log.WithName("controller_bazelbuildserver")

/**
* USER ACTION REQUIRED: This is a scaffold file intended for the user to modify with their own Controller
* business logic.  Delete these comments after modifying this file.*
 */

// Add creates a new BazelBuildServer Controller and adds it to the Manager. The Manager will set fields on the Controller
// and Start it when the Manager is Started.
func Add(mgr manager.Manager) error {
	return add(mgr, newReconciler(mgr))
}

// newReconciler returns a new reconcile.Reconciler
func newReconciler(mgr manager.Manager) reconcile.Reconciler {
	return &ReconcileBazelBuildServer{client: mgr.GetClient(), scheme: mgr.GetScheme()}
}

// add adds a new Controller to mgr with r as the reconcile.Reconciler
func add(mgr manager.Manager, r reconcile.Reconciler) error {
	// Create a new controller
	c, err := controller.New("bazelbuildserver-controller", mgr, controller.Options{Reconciler: r})
	if err != nil {
		return err
	}

	// Watch for changes to primary resource BazelBuildServer
	err = c.Watch(&source.Kind{Type: &buildv1alpha1.BazelBuildServer{}}, &handler.EnqueueRequestForObject{})
	if err != nil {
		return err
	}

	// TODO(user): Modify this to be the types you create that are owned by the primary resource
	// Watch for changes to secondary resource Pods and requeue the owner BazelBuildServer
	err = c.Watch(&source.Kind{Type: &corev1.Pod{}}, &handler.EnqueueRequestForOwner{
		IsController: true,
		OwnerType:    &buildv1alpha1.BazelBuildServer{},
	})
	if err != nil {
		return err
	}

	return nil
}

// blank assignment to verify that ReconcileBazelBuildServer implements reconcile.Reconciler
var _ reconcile.Reconciler = &ReconcileBazelBuildServer{}

// ReconcileBazelBuildServer reconciles a BazelBuildServer object
type ReconcileBazelBuildServer struct {
	// This client, initialized using mgr.Client() above, is a split client
	// that reads objects from the cache and writes to the apiserver
	client client.Client
	scheme *runtime.Scheme
}

// Reconcile reads that state of the cluster for a BazelBuildServer object and makes changes based on the state read
// and what is in the BazelBuildServer.Spec
// TODO(user): Modify this Reconcile function to implement your Controller logic.  This example creates
// a Pod as an example
// Note:
// The Controller will requeue the Request to be processed again if the returned error is non-nil or
// Result.Requeue is true, otherwise upon completion it will remove the work from the queue.
func (r *ReconcileBazelBuildServer) Reconcile(request reconcile.Request) (reconcile.Result, error) {
	reqLogger := log.WithValues("Request.Namespace", request.Namespace, "Request.Name", request.Name)
	reqLogger.Info("Reconciling BazelBuildServer")

	// Fetch the BazelBuildServer instance
	instance := &buildv1alpha1.BazelBuildServer{}
	err := r.client.Get(context.TODO(), request.NamespacedName, instance)
	if err != nil {
		if errors.IsNotFound(err) {
			// Request object not found, could have been deleted after reconcile request.
			// Owned objects are automatically garbage collected. For additional cleanup logic use finalizers.
			// Return and don't requeue
			return reconcile.Result{}, nil
		}
		// Error reading the object - requeue the request.
		return reconcile.Result{}, err
	}

	// Populate any missing values with defaults after finding the resource
	changed := instance.SetDefaults()
	if changed {
		err = r.client.Update(context.TODO(), instance)
		if err != nil {
			return reconcile.Result{}, err
		}
		reqLogger.Info("BazelBuildServer resource updated with defaults")
		// Expect for re-trigger
		return reconcile.Result{}, nil
	}

	// Define a new ConfigMap object
	configMap := bazelBuildfarmConfigMap(instance)

	// Set BazelBuildServer instance as the owner and controller
	if err := controllerutil.SetControllerReference(instance, configMap, r.scheme); err != nil {
		return reconcile.Result{}, err
	}

	// Check if this ConfigMap already exists
	foundConfigMap := &corev1.ConfigMap{}
	err = r.client.Get(context.TODO(), types.NamespacedName{Name: configMap.Name, Namespace: configMap.Namespace}, foundConfigMap)
	if err != nil && errors.IsNotFound(err) {
		reqLogger.Info("Creating a new ConfigMap", "ConfigMap.Namespace", configMap.Namespace, "ConfigMap.Name", configMap.Name)
		err = r.client.Create(context.TODO(), configMap)
		if err != nil {
			return reconcile.Result{}, err
		}

		// ConfigMap created successfully - don't requeue, just continue
	} else if err != nil {
		return reconcile.Result{}, err
	}

	// ConfigMap already exists - don't requeue
	reqLogger.Info("Skip reconcile: ConfigMap already exists", "ConfigMap.Namespace", foundConfigMap.Namespace, "ConfigMap.Name", foundConfigMap.Name)

	// Define a new server Deployment object
	deployment := bazelBuildfarmServerDeployment(instance)

	// Set BazelBuildServer instance as the owner and controller
	if err := controllerutil.SetControllerReference(instance, deployment, r.scheme); err != nil {
		return reconcile.Result{}, err
	}

	// Check if this Deployment already exists
	foundDeployment := &appsv1.Deployment{}
	err = r.client.Get(context.TODO(), types.NamespacedName{Name: deployment.Name, Namespace: deployment.Namespace}, foundDeployment)
	if err != nil && errors.IsNotFound(err) {
		reqLogger.Info("Creating a new Deployment", "Deployment.Namespace", deployment.Namespace, "Deployment.Name", deployment.Name)
		err = r.client.Create(context.TODO(), deployment)
		if err != nil {
			return reconcile.Result{}, err
		}

		// Deployment created successfully - don't requeue
		return reconcile.Result{}, nil
	} else if err != nil {
		return reconcile.Result{}, err
	}

	// Deployment already exists - don't requeue
	reqLogger.Info("Skip reconcile: Deployment already exists", "Deployment.Namespace", foundDeployment.Namespace, "Deployment.Name", foundDeployment.Name)
	return reconcile.Result{}, nil
}

func bazelBuildfarmWorkerConfigMap(cr *buildv1alpha1.BazelBuildServer) *corev1.ConfigMap {
	return &corev1.ConfigMap{
		ObjectMeta: metav1.ObjectMeta{
			Name:      cr.Name + "-worker-config",
			Namespace: cr.Namespace,
			Labels: map[string]string{
				"app":       cr.Name,
				"component": "worker",
			},
		},
		Data: map[string]string{
			"shard-worker.config": fmt.Sprintf(`
# the listening port of the shard-worker grpc server
port: 8981

# the host:port of this grpc server, required to be accessible
# by all shard cluster servers
public_name: "localhost:8981"

# the digest function for this worker, required
# to match out of band between the client and server
# since resource names must be determined on the client
# for a valid upload
digest_function: SHA256

# all content for the operations will be stored under this path
root: "/tmp/worker"

# total size in bytes of inline content for action results
# output files, stdout, and stderr content, in that order
# will be inlined if their cumulative size does not exceed this limit.
inline_content_limit: 1048567 # 1024 * 1024

# the period between poll operations at any stage
operation_poll_period: {
	seconds: 1
	nanos: 0
}

# key/value set of definining capabilities of this worker
# all execute requests must match perfectly with workers which
# provide capabilities
# so an action with a required platform: { arch: "x86_64" } must
# match with a worker with at least { arch: "x86_64" } here
platform: {
	# commented out here for illustrative purposes, a default empty
	# 'platform' is a sufficient starting point without specifying
	# any platform requirements on the actions' side
	###
	# property: {
	#   name: "key_name"
	#   value: "value_string"
	# }
}

# the worker CAS configuration
cas: {
	# present a filesystem CAS, required to host an exec root
	# the first filesystem CAS in the configuration will be used
	# as the storage for an exec filesystem for an operation
	filesystem: {
		# the local cache location relative to the 'root', or absolute
		path: "cache"

		# limit for contents of files retained
		# from CAS in the cache
		max_size_bytes: 2147483648 # 2 * 1024 * 1024 * 1024

		# limit for content size of files retained
		# from CAS in the cache
		max_entry_size_bytes: 2147483648 # 2 * 1024 * 1024 * 1024
	}
}

# another cas entry specification here will provide a fallback
# for the filesystem cas. Any number of consecutive filesystem
# fallbacks may be used, terminated with zero or one of grpc or
# memory types.
#
#cas: {
#  grpc: {
#      instance_name: "external-cas",
#      target: "cas.external.com:1234",
#  }
#}

# the number of concurrently available slots in the execute phase
execute_stage_width: 1

# the number of concurrently available slots in the input fetch phase
input_fetch_stage_width: 1

# Use an input directory creation strategy which creates a single
# directory tree at the highest level of the input tree containing
# no output paths of any kind, and symlinks that directory into an
# action's execroot, potentially saving large amounts of time
# spent manufacturing the same read-only input hierarchy over
# multiple actions' executions.
link_input_directories: true

# an imposed action-key-invariant timeout used in the unspecified timeout case
default_action_timeout: {
	seconds: 600
	nanos: 0
}

# a limit on the action timeout specified in the action, above which
# the operation will report a failed result immediately
maximum_action_timeout: {
	seconds: 3600
	nanos: 0
}

# prefix command executions with this path
#execution_policies: {
#  name: "test"
#  wrapper: {
#    path: "/path/to/execution/wrapper"
#  }
#}

# A backplane specification hosted with redis cluster
# Fields omitted are expected defaults, and are unused or undesirable on the workers
redis_shard_backplane_config: {
	# The URI of the redis cluster endpoint. This must
	# be a single URI, regardless of the layout of the cluster
	redis_uri: "redis://localhost:6379"

	# The size of the redis connection pool
	jedis_pool_max_total: 4000

	# The redis key used to store a hash of registered Workers
	# to their registration expiration time. After a worker's
	# registration has expired, they are no longer considered
	# as shards of the CAS
	workers_hash_name: "Workers"

	# A redis pubsub channel key where changes to the cluster
	# membership are announced
	worker_channel: "WorkerChannel"

	# A redis key prefix for all ActionCache entries, suffixed
	# with the action's key and mapping to an ActionResult
	action_cache_prefix: "ActionCache"

	# The ttl maintained for ActionCache entries. This is not
	# refreshed on getActionResult hit
	action_cache_expire: 2419200 # 4 weeks

	# A redis key prefix for all blacklisted actions, suffixed
	# with the action's key hash. An action which is blacklisted
	# should be rejected for all requests where it is identified via
	# its RequestMetadata
	# To meet API standards, a request which matches this condition
	# receives a transient UNAVAILABLE response, which, in bazel's
	# case, can induce a fallback to non-remote recovery, rather
	# than a catastrophic failure.
	action_blacklist_prefix: "ActionBlacklist"

	# The ttl maintained for action_blacklist entries.
	action_blacklist_expire: 3600 # 1 hour

	# A redis key prefix for all Operations, suffixed with the
	# operation's name and mapping to an Operation which reflects
	# the cluster perceived state of that Operation
	operation_prefix: "Operation"

	# The ttl maintained for all Operations, updated on each
	# modification
	operation_expire: 604800 # 1 week

	# The redis key used to store a list of QueueEntrys
	# awaiting execution by workers. These are queued
	# by an operation_queuer agent, and dequeued by a worker.
	# Redis keyspace manipulation is used here to support multi-
	# key commands.
	# The string contained within {} must match that of
	# dispatching_list_name.
	queued_operations_list_name: "{Execution}:QueuedOperations"

	# The redis key of a list used to ensure reliable processing of
	# ready-to-run queue entries together with operation watch
	# monitoring.
	# The string contained within {} must match that of
	# queued_operations_list_name.
	dispatching_list_name: "{Execution}:DispatchingOperations"

	# A redis key prefix for operations which are being dequeued
	# from the ready-to-run queue. The key is suffixed with the
	# operation name and contains the expiration time in epoch
	# milliseconds after which the operation is considered lost.
	dispatching_prefix: "Dispatching"

	# The delay in milliseconds used to populate dispathing operation
	# entries
	dispatching_timeout_millis: 10000

	# The redis key of a hash of operation names to the worker
	# lease for its execution. Entries in this hash are monitored
	# by the dispatched_monitor for expiration, and the worker
	# is expected to extend a lease in a timely fashion to indicate
	# continued handling of an operation.
	dispatched_operations_hash_name: "DispatchedOperations"

	# A redis pubsub channel prefix suffixed by an operation name
	# where updates and keepalives are transmitted as it makes its
	# way through the various processing elements of the cluster.
	operation_channel_prefix: "OperationChannel"

	# A redis key prefix suffixed with a blob digest that maps to a
	# set of workers which advertise that blob's availability.
	# This set must be intersected with the set of active worker
	# leases to be considered meaningful.
	cas_prefix: "ContentAddressableStorage"

	# The ttl maintained for cas entries. This is not refreshed on
	# any read access of the blob.
	cas_expire: 604800 # 1 week

	# Enable an agent in the backplane client which subscribes
	# to worker_channel and operation_channel events. If this is
	# disabled, the responsiveness of watchers is reduced and the
	# CAS is reduced.
	# When in doubt, leave this enabled.
	subscribe_to_backplane: true
}
		`),
		},
	}
}

// bazelBuildfarmConfigMap returns a ConfigMap to configure bazel with the same name/namespace as the cr
func bazelBuildfarmConfigMap(cr *buildv1alpha1.BazelBuildServer) *corev1.ConfigMap {
	return &corev1.ConfigMap{
		ObjectMeta: metav1.ObjectMeta{
			Name:      cr.Name + "-config",
			Namespace: cr.Namespace,
			Labels: map[string]string{
				"app": cr.Name,
			},
		},
		Data: map[string]string{
			"shard-server.config": fmt.Sprintf(`
instances {
	name: "shard"

	# the digest function for this instance, required
	# to match out of band between the client and server
	# since resource names must be determined on the client
	# for a valid upload
	digest_function: SHA256

	# the implicit type specifier for this instance
	# a shard instance is a cooperative member of a cluster
	# that communicates over a backplane for its operation
	# a shard instance implies a backend-retained ActionCache,
	# Operation store, and CAS index and registration, with
	# Workers representing egalitarian shards of the CAS
	shard_instance_config: {
		# Enable an agent in this instance to monitor the
		# Operation store to ensure that dispatched Operations
		# with expired worker leases are requeued
		# At least one agent within the cluster must exist to
		# ensure that worker leases on operations are handled.
		# While not exclusive, it is safe to run the monitor
		# on multiple instances concurrently.
		run_dispatched_monitor: true

		# The interval at which to run the dispatch monitor's
		# lease expiration check
		dispatched_monitor_interval_seconds: 1

		# Enable an agent in this instance to acquire Execute
		# request entries cooperatively from an arrival queue
		# on the backplane.
		# If true, this instance will validate and transform
		# an arrival queue ExecuteRequest as an ExecuteEntry into
		# a QueueEntry with a heavyweight ready-to-run QueuedOperation
		# available in the CAS via a fixed size.
		# At least one agent within the cluster must exist to
		# bring operations from the arrival queue to the ready-to-run queue,
		# or no operation execution will take place.
		# The operation queuer is exclusive and should run on
		# multiple instances concurrently.
		run_operation_queuer: true

		# The maximum size of a single blob accepted via a
		# ByteStream#write or ContentAddressableStorage#batchUpdateBlobs
		# To meet API standards, a request which exceeds this size receives
		# a transient UNAVAILABLE response, which, in bazel's case, induces
		# a fallback to non-remote recovery, rather than a catastrophic
		# failure.
		max_blob_size: 4294967296

		# A backplane specification hosted with redis cluster
		redis_shard_backplane_config: {
			# The URI of the redis cluster endpoint. This must
			# be a single URI, regardless of the layout of the cluster
			redis_uri: "redis://localhost:6379"

			# The size of the redis connection pool
			jedis_pool_max_total: 4000

			# The redis key used to store a hash of registered Workers
			# to their registration expiration time. After a worker's
			# registration has expired, they are no longer considered
			# as shards of the CAS
			workers_hash_name: "Workers"

			# A redis pubsub channel key where changes to the cluster
			# membership are announced
			worker_channel: "WorkerChannel"

			# A redis key prefix for all ActionCache entries, suffixed
			# with the action's key and mapping to an ActionResult
			action_cache_prefix: "ActionCache"

			# The ttl maintained for ActionCache entries. This is not
			# refreshed on getActionResult hit
			action_cache_expire: 2419200 # 4 weeks

			# A redis key prefix for all blacklisted actions, suffixed
			# with the action's key hash. An action which is blacklisted
			# should be rejected for all requests where it is identified via
			# its RequestMetadata
			# To meet API standards, a request which matches this condition
			# receives a transient UNAVAILABLE response, which, in bazel's
			# case, can induce a fallback to non-remote recovery, rather
			# than a catastrophic failure.
			action_blacklist_prefix: "ActionBlacklist"

			# The ttl maintained for action_blacklist entries.
			action_blacklist_expire: 3600 # 1 hour

			# A redis key prefix for all Operations, suffixed with the
			# operation's name and mapping to an Operation which reflects
			# the cluster perceived state of that Operation
			operation_prefix: "Operation"

			# The ttl maintained for all Operations, updated on each
			# modification
			operation_expire: 604800 # 1 week

			# The redis key used to store a list of ExecuteEntrys
			# awaiting transformation into QueueEntrys. These are queued
			# by an instance which receives an ExecuteRequest, and
			# dequeued by an operation_queuer agent.
			# Redis keyspace manipulation is used here to support multi-
			# key commands.
			# The string contained within {} must match that of
			# processing_list_name.
			pre_queued_operations_list_name: "{Arrival}:PreQueuedOperations"

			# The redis key of a list used to ensure reliable processing of
			# arrival queue entries together with operation watch monitoring.
			# The string contained within {} must match that of
			# pre_queued_operations_list_name.
			processing_list_name: "{Arrival}:ProcessingOperations"

			# A redis key prefix for operations which are being dequeued
			# from the arrival queue. The key is suffixed with the operation
			# name and contains the expiration time in epoch milliseconds
			# after which the operation is considered lost.
			processing_prefix: "Processing"

			# The delay in milliseconds used to populate processing operation
			# entries
			processing_timeout_millis: 10000

			# The redis key used to store a list of QueueEntrys
			# awaiting execution by workers. These are queued
			# by an operation_queuer agent, and dequeued by a worker.
			# Redis keyspace manipulation is used here to support multi-
			# key commands.
			# The string contained within {} must match that of
			# dispatching_list_name.
			queued_operations_list_name: "{Execution}:QueuedOperations"

			# The redis key of a list used to ensure reliable processing of
			# ready-to-run queue entries together with operation watch
			# monitoring.
			# The string contained within {} must match that of
			# queued_operations_list_name.
			dispatching_list_name: "{Execution}:DispatchingOperations"

			# A redis key prefix for operations which are being dequeued
			# from the ready-to-run queue. The key is suffixed with the
			# operation name and contains the expiration time in epoch
			# milliseconds after which the operation is considered lost.
			dispatching_prefix: "Dispatching"

			# The delay in milliseconds used to populate dispathing operation
			# entries
			dispatching_timeout_millis: 10000

			# The redis key of a hash of operation names to the worker
			# lease for its execution. Entries in this hash are monitored
			# by the dispatched_monitor for expiration, and the worker
			# is expected to extend a lease in a timely fashion to indicate
			# continued handling of an operation.
			dispatched_operations_hash_name: "DispatchedOperations"

			# A redis pubsub channel prefix suffixed by an operation name
			# where updates and keepalives are transmitted as it makes its
			# way through the various processing elements of the cluster.
			operation_channel_prefix: "OperationChannel"

			# A redis key prefix suffixed with a blob digest that maps to a
			# set of workers which advertise that blob's availability.
			# This set must be intersected with the set of active worker
			# leases to be considered meaningful.
			cas_prefix: "ContentAddressableStorage"

			# The ttl maintained for cas entries. This is not refreshed on
			# any read access of the blob.
			cas_expire: 604800 # 1 week

			# Enable an agent in the backplane client which subscribes
			# to worker_channel and operation_channel events. If this is
			# disabled, the responsiveness of watchers is reduced and the
			# CAS is reduced.
			# When in doubt, leave this enabled.
			subscribe_to_backplane: true

			# Enable an agent in the backplane client which monitors
			# watched operations and ensures that they are in a known
			# maintained, or expirable state.
			# This field is intended to distinguish servers with active
			# listeners of the backplane from workers with passive reuse
			# of it.
			# When in doubt, leave this enabled on servers.
			run_failsafe_operation: true

			# The maximum length that the ready-to-run queue is allowed
			# to reach via queue invocations. Reaching this limit induces
			# back-pressure on the arrival queue and is intended as a
			# flow control mechanism for execution.
			# Average QueuedOperation size in relation to CAS size and
			# churn should influence safe values here.
			max_queue_depth: 100000

			# The maximum length that the arrival queue is allowed
			# to reach via execute invocations. Reaching this limit results
			# in Execute errors of RESOURCE_EXHAUSTED and is intended as a
			# safety check for the backplane storage.
			# Redis cluster storage size should influence safe values here.
			max_pre_queue_depth: 1000000
		}
	}
}

# the listening port of the buildfarm grpc server
port: %d

# the instance to which all requests with an empty/missing
# instance name are routed
#
# this can be empty as well, to indicate that there is no
# default instance
default_instance_name: "shard"
			`, cr.Spec.BuildfarmServerDeploymentSpec.BazelBuildfarmContainerSpec.BazelBuildfarmPort),
		},
	}
}

func bazelBuildfarmWorkerDeployment(cr *buildv1alpha1.BazelBuildServer) *appsv1.Deployment {
	return &appsv1.Deployment{
		ObjectMeta: metav1.ObjectMeta{
			Name:      cr.Name + "-server-deployment",
			Namespace: cr.Namespace,
			Labels: map[string]string{
				"app": cr.Name,
			},
		},
		Spec: appsv1.DeploymentSpec{
			Replicas: &replicas,
			Selector: &metav1.LabelSelector{
				MatchLabels: map[string]string{
					"app":       cr.Name,
					"component": "buildfarm-server",
				},
			},
			Template: corev1.PodTemplateSpec{
				ObjectMeta: metav1.ObjectMeta{
					Labels: map[string]string{
						"app":       cr.Name,
						"component": "buildfarm-server",
					},
				},
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{
						corev1.Container{
							Ports: []corev1.ContainerPort{
								corev1.ContainerPort{
									ContainerPort: cr.Spec.BuildfarmServerDeploymentSpec.BazelBuildfarmContainerSpec.BazelBuildfarmPort,
									Protocol:      corev1.ProtocolTCP,
								},
							},
							LivenessProbe: &corev1.Probe{
								Handler: corev1.Handler{
									TCPSocket: &corev1.TCPSocketAction{
										Port: intstr.IntOrString{Type: intstr.Int, IntVal: cr.Spec.BuildfarmServerDeploymentSpec.BazelBuildfarmContainerSpec.BazelBuildfarmPort},
									},
								},
							},
							Image: *cr.Spec.BuildfarmServerDeploymentSpec.BazelBuildfarmContainerSpec.BazelBuildfarmImage,
							Name:  "buildfarm-server",
							Args: []string{
								"/var/lib/buildfarm-server/shard-server.config",
								"-p",
								strconv.Itoa(int(cr.Spec.BuildfarmServerDeploymentSpec.BazelBuildfarmContainerSpec.BazelBuildfarmPort)),
							},
							VolumeMounts: []corev1.VolumeMount{
								corev1.VolumeMount{
									Name:      "config",
									MountPath: "/var/lib/buildfarm-server",
									ReadOnly:  true,
								},
							},
						},
						corev1.Container{
							Name:  "buildfarm-redis",
							Image: *cr.Spec.BuildfarmServerDeploymentSpec.RedisSpec.RedisImage,
						},
					},
					Volumes: []corev1.Volume{
						corev1.Volume{
							Name: "config",
							VolumeSource: corev1.VolumeSource{
								ConfigMap: &corev1.ConfigMapVolumeSource{
									LocalObjectReference: corev1.LocalObjectReference{
										Name: cr.Name + "-config",
									},
								},
							},
						},
					},
				},
			},
		},
	}
}

// bazelBuildfarmServerDeployment returns a Bazel deployment with the same name/namespace as the cr
func bazelBuildfarmServerDeployment(cr *buildv1alpha1.BazelBuildServer) *appsv1.Deployment {
	return &appsv1.Deployment{
		ObjectMeta: metav1.ObjectMeta{
			Name:      cr.Name + "-server-deployment",
			Namespace: cr.Namespace,
			Labels: map[string]string{
				"app": cr.Name,
			},
		},
		Spec: appsv1.DeploymentSpec{
			Replicas: &replicas,
			Selector: &metav1.LabelSelector{
				MatchLabels: map[string]string{
					"app":       cr.Name,
					"component": "buildfarm-server",
				},
			},
			Template: corev1.PodTemplateSpec{
				ObjectMeta: metav1.ObjectMeta{
					Labels: map[string]string{
						"app":       cr.Name,
						"component": "buildfarm-server",
					},
				},
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{
						corev1.Container{
							Ports: []corev1.ContainerPort{
								corev1.ContainerPort{
									ContainerPort: cr.Spec.BuildfarmServerDeploymentSpec.BazelBuildfarmContainerSpec.BazelBuildfarmPort,
									Protocol:      corev1.ProtocolTCP,
								},
							},
							LivenessProbe: &corev1.Probe{
								Handler: corev1.Handler{
									TCPSocket: &corev1.TCPSocketAction{
										Port: intstr.IntOrString{Type: intstr.Int, IntVal: cr.Spec.BuildfarmServerDeploymentSpec.BazelBuildfarmContainerSpec.BazelBuildfarmPort},
									},
								},
							},
							Image: *cr.Spec.BuildfarmServerDeploymentSpec.BazelBuildfarmContainerSpec.BazelBuildfarmImage,
							Name:  "buildfarm-server",
							Args: []string{
								"/var/lib/buildfarm-server/shard-server.config",
								"-p",
								strconv.Itoa(int(cr.Spec.BuildfarmServerDeploymentSpec.BazelBuildfarmContainerSpec.BazelBuildfarmPort)),
							},
							VolumeMounts: []corev1.VolumeMount{
								corev1.VolumeMount{
									Name:      "config",
									MountPath: "/var/lib/buildfarm-server",
									ReadOnly:  true,
								},
							},
						},
						corev1.Container{
							Name:  "buildfarm-redis",
							Image: *cr.Spec.BuildfarmServerDeploymentSpec.RedisSpec.RedisImage,
						},
					},
					Volumes: []corev1.Volume{
						corev1.Volume{
							Name: "config",
							VolumeSource: corev1.VolumeSource{
								ConfigMap: &corev1.ConfigMapVolumeSource{
									LocalObjectReference: corev1.LocalObjectReference{
										Name: cr.Name + "-config",
									},
								},
							},
						},
					},
				},
			},
		},
	}
}
