package controller

import (
	"gitlab.com/kjanania/bazel-buildfarm-operator/pkg/controller/bazelbuild"
)

func init() {
	// AddToManagerFuncs is a list of functions to create controllers and add them to a manager.
	AddToManagerFuncs = append(AddToManagerFuncs, bazelbuild.Add)
}
